﻿using System;
using System.Threading.Tasks;
using CreditOrganization.Fake.Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yes.CreditApplication.Api.Contracts.Admin;
using Yes.CreditApplication.Api.Contracts.CreditOrganizations;
using CreditApplicationRequest = Yes.CreditApplication.Api.Contracts.CreditOrganizations.CreditApplicationRequest;

namespace CreditOrganization.Fake.Controllers
{
    [Route("api/v1/credit-applications")]
    public class CreditApplicationsController : ControllerBase
    {
        private readonly ICreditApplicationManager manager;

        public CreditApplicationsController(ICreditApplicationManager manager)
        {
            this.manager = manager;
        }

        /// <summary>
        /// Подтверждает заявку на кредит
        /// </summary>
        [HttpPost("{creditApplicationId}/confirmation")]
        [ProducesResponseType(typeof(CreditApplicationResponse), StatusCodes.Status200OK)]
        public async Task<IActionResult> ConfirmCreditApplication([FromRoute]Guid creditApplicationId, CreditApplicationRequest model)
        {
            var response = await manager.ConfirmCreditApplication(creditApplicationId, model);
            return MakeResponse(response);
        }
        
        /// <summary>
        /// Запрашивает статус заявки на кредит
        /// </summary>
        [HttpPost("{creditApplicationId}/status")]
        [ProducesResponseType(typeof(CreditApplicationResponse), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetCreditApplicationStatus([FromRoute]Guid creditApplicationId, CreditApplicationRequest model)
        {
            var response = await manager.GetCreditApplicationStatus(creditApplicationId, model);
            return MakeResponse(response);
        }
    }
}