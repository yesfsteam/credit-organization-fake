﻿using System;
using System.Threading.Tasks;
using CreditOrganization.Fake.Models.Configuration;
using Microsoft.Extensions.Logging;
using SD.Cqrs;
using SD.Messaging.Models;
using Yes.Contracts;
using Yes.CreditApplication.Api.Contracts;
using Yes.CreditApplication.Api.Contracts.Commands;
using Yes.CreditApplication.Api.Contracts.CreditOrganizations;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.CreditApplication.Api.Contracts.Events;
using Yes.Infrastructure.Common.Extensions;
using Yes.Infrastructure.Http;
using CreditApplicationRequest = Yes.CreditApplication.Api.Contracts.CreditOrganizations.CreditApplicationRequest;

namespace CreditOrganization.Fake.Domain
{
    public interface ICreditApplicationManager
    {
        Task<ProcessingResult> CheckCreditApplication(CreditApplicationCreatedEvent @event);
        Task<Response<CreditApplicationResponse>> ConfirmCreditApplication(Guid creditApplicationId, CreditApplicationRequest model);
        Task<Response<CreditApplicationResponse>> GetCreditApplicationStatus(Guid creditApplicationId, CreditApplicationRequest model);
    }

    public class CreditApplicationManager : ICreditApplicationManager
    {
        private readonly ILogger<CreditApplicationManager> logger;
        private readonly ICommandSender commandSender;
        private readonly ApplicationConfiguration configuration;
        private readonly MessagingConfiguration messagingConfiguration;

        public CreditApplicationManager(ILogger<CreditApplicationManager> logger, ICommandSender commandSender, 
            ApplicationConfiguration configuration, MessagingConfiguration messagingConfiguration)
        {
            this.logger = logger;
            this.commandSender = commandSender;
            this.configuration = configuration;
            this.messagingConfiguration = messagingConfiguration;
        }

        public async Task<ProcessingResult> CheckCreditApplication(CreditApplicationCreatedEvent @event)
        {
            var beginTime = DateTime.Now;
            try
            {
                var randValue = new Random().Next(0,2);
                var command = publishDecisionCommand(
                    beginTime, 
                    randValue == 0 ? CreditOrganizationRequestStatus.Approved : CreditOrganizationRequestStatus.Declined, 
                    @event.CreditApplicationId,
                    randValue == 0 ? null : "Timeout");
                logger.LogInformation($"Check request accepted. Duration: {beginTime.GetDuration()} Request: {@event}, Command: {command}");
                return await Task.FromResult(ProcessingResult.Ok());
            }
            catch (Exception e)
            {
                if (@event.RetryCount == messagingConfiguration.MaxRetryCount - 1)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ServerError, @event.CreditApplicationId,
                        $"Количество попыток обработки: {messagingConfiguration.MaxRetryCount}. Детали ошибки в логах приложения");
                    logger.LogError(e, $"Error while processing check request. Max retry count ({messagingConfiguration.MaxRetryCount}) exceeded, finish processing. Duration: {beginTime.GetDuration()} Request: {@event}, Command: {command}");
                    return ProcessingResult.Ok();
                }

                logger.LogError(e, $"Error while processing check request, try again later. Duration: {beginTime.GetDuration()} Request: {@event}");
                return ProcessingResult.Fail();
            }
        }
        
        private CreditApplicationDecisionCommand publishDecisionCommand(DateTime createdDate, CreditOrganizationRequestStatus status, Guid creditApplicationId, string description = null)
        {
            var command = new CreditApplicationDecisionCommand
            {
                Date = createdDate,
                Status = status,
                CreditApplicationId = creditApplicationId,
                CreditOrganizationId = configuration.CreditOrganizationId,
                Details = description
            };
            commandSender.SendCommand(command, BoundedContexts.CREDIT_APPLICATION_API, Routes.Decisions);
            return command;
        }

        public async Task<Response<CreditApplicationResponse>> ConfirmCreditApplication(Guid creditApplicationId, CreditApplicationRequest model)
        {
            var beginTime = DateTime.Now;
            try
            {
                var randValue = new Random().Next(0,2);
                var response = new CreditApplicationResponse
                {
                    Date = beginTime,
                    Status = randValue == 0 ? CreditOrganizationRequestStatus.Confirmed : CreditOrganizationRequestStatus.ServerError,
                    Details = randValue == 0 ? null : "У нас ничего не работает"
                };
                logger.LogInformation($"Confirmation request accepted. Duration: {beginTime.GetDuration()} Request: {model}, Response: {response}");
                return await Task.FromResult(Response<CreditApplicationResponse>.Ok(response));
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while processing confirmation request. Duration: {beginTime.GetDuration()} Request: {model}");
                return Response<CreditApplicationResponse>.Ok(new CreditApplicationResponse
                {
                    Status = CreditOrganizationRequestStatus.ServerError,
                    Details = "Детали ошибки в логах приложения"
                });
            }
        }

        public async Task<Response<CreditApplicationResponse>> GetCreditApplicationStatus(Guid creditApplicationId, CreditApplicationRequest model)
        {
            var beginTime = DateTime.Now;
            try
            {
                var randValue = new Random().Next(0,2);
                var response = new CreditApplicationResponse
                {
                    Date = beginTime,
                    Status = randValue == 0 ? CreditOrganizationRequestStatus.Approved : CreditOrganizationRequestStatus.ServerError,
                    Details = randValue == 0 ? null : "У нас ничего не работает"
                };
                logger.LogInformation($"Get status. Duration: {beginTime.GetDuration()} Request: {model}, Response: {response}");
                return await Task.FromResult(Response<CreditApplicationResponse>.Ok(response));
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while processing get status request. Duration: {beginTime.GetDuration()} Request: {model}");
                return Response<CreditApplicationResponse>.Ok(new CreditApplicationResponse
                {
                    Status = CreditOrganizationRequestStatus.ServerError,
                    Details = "Детали ошибки в логах приложения"
                });
            }
        }
    }
}