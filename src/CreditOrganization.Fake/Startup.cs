using System.IO;
using CreditOrganization.Fake.Domain;
using CreditOrganization.Fake.Extensions;
using CreditOrganization.Fake.Messaging;
using CreditOrganization.Fake.Models.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.OpenApi.Models;
using SD.Cqrs;
using SD.Cqrs.NetCore;
using SD.Logger.Serilog.NetCore;
using SD.Messaging.RabbitMQ.NetCore;
using Serilog;
using Yes.Contracts;
using Yes.CreditApplication.Api.Contracts;
using Yes.CreditApplication.Api.Contracts.Commands;
using Yes.CreditApplication.Api.Contracts.Events;

namespace CreditOrganization.Fake
{
    public class Startup
    {
	    private readonly string applicationName;
	    
	    public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;
            applicationName = typeof(Program).Assembly.GetName().Name;
        }

        private readonly IConfiguration configuration;
        
        public void ConfigureServices(IServiceCollection services)
        {
	        var messagingConfiguration = configuration.BindFromAppConfig<MessagingConfiguration>();
	        services.AddSingleton(messagingConfiguration);
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            services.AddRouting(options => options.LowercaseUrls = true);

            services.AddCqrsEngine()
	            .WithSerilogLogger()
	            .WithRabbitMQMessagingEngine()
	            .WithBoundedContext(BoundedContext.Create(BoundedContexts.CREDIT_ORGANIZATION_FAKE)
		            .ListeningEvents(typeof(CreditApplicationCreatedEvent)).From(BoundedContexts.CREDIT_APPLICATION_API).WithMaxRetry(messagingConfiguration.MaxRetryCount)
		            .PublishingCommands(typeof(CreditApplicationDecisionCommand)).To(BoundedContexts.CREDIT_APPLICATION_API).On(Routes.Decisions)
		            .Build())
	            .AddProjection<CreditApplicationProjection>(BoundedContexts.CREDIT_APPLICATION_API, messagingConfiguration.ChannelsCount);
            
            services.AddTransient<ICreditApplicationManager, CreditApplicationManager>();
            services.AddSingleton(configuration.BindFromAppConfig<ApplicationConfiguration>());
            
            if (configuration.GetValue<bool>("EnableSwagger"))
	            services.AddSwaggerGen(c =>
	            {
		            c.SwaggerDoc("v1", new OpenApiInfo { Title = applicationName, Version = "v1" });
		            c.IncludeXmlComments(Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, $"{applicationName}.xml"));
	            });


			Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(configuration).CreateLogger();
		}

        public void Configure(IApplicationBuilder app, IHostApplicationLifetime applicationLifetime, CqrsEngine cqrsEngine)
        {
	        applicationLifetime.ApplicationStopping.Register(()=>OnApplicationStopping(cqrsEngine));

            app.UseRouting();
            app.UseEndpoints(endpoints => endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}"));

            if (configuration.GetValue<bool>("EnableSwagger"))
            {
	            app.UseSwagger();
	            app.UseSwaggerUI(c =>
	            {
		            c.SwaggerEndpoint("/swagger/v1/swagger.json", $"{applicationName} v1");
		            c.RoutePrefix = string.Empty;
	            });
            }
            
            cqrsEngine.Start();
            
            Log.Logger.Information($"{applicationName} has been started");
        }

        private void OnApplicationStopping(CqrsEngine cqrsEngine)
        {
	        cqrsEngine.Stop();
	        Log.Logger.Information($"{applicationName} has been stopped");
        }
	}
}