﻿using System.Threading.Tasks;
using CreditOrganization.Fake.Domain;
using SD.Cqrs;
using SD.Messaging.Models;
using Yes.CreditApplication.Api.Contracts.Events;

namespace CreditOrganization.Fake.Messaging
{
    public class CreditApplicationProjection : Projection
    {
        private readonly ICreditApplicationManager manager;

        public CreditApplicationProjection(ICreditApplicationManager manager) 
        {
            this.manager = manager; 
        }

        public async Task<ProcessingResult> Handle(CreditApplicationCreatedEvent command)
            => await manager.CheckCreditApplication(command);
    }
}