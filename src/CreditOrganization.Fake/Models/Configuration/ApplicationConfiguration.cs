﻿using System;

namespace CreditOrganization.Fake.Models.Configuration
{
    public class ApplicationConfiguration
    {
        public Guid CreditOrganizationId { get; set; }
    }
}